<#
.SYNOPSIS
  Script will iterate through Veeam backup / Replica jobs and output and issues relating to them

.DESCRIPTION
  Script will iterate through Veeam backup / Replica jobs and output and issues relating to them

.PARAMETER slowBackupHrs
  Hours that a backup job has been running before it is counted as slow (Default: 8)

.PARAMETER slowReplicationHrs
  Hours that a replica job has been running before it is counted as slow (Default: 1)

.PARAMETER slowTapeDays
  Days that a tape job has been running before it is counted as slow (Default: 3)

.PARAMETER slowCopyHrs
  Hours that a copy job has been running before it is counted as slow (Default: 8)

.PARAMETER logDebug
  A switch that determines if debug logging is turned on. (Default: false)

.INPUTS
  None

.OUTPUTS Log File
  The script log file stored in c:\Program Files\Xymon\veeam.log

.NOTES
  Version:        1.0
  Author:         Michael Smith
  Creation Date:  13-Mar-2017
  Purpose/Change: Initial script development

.EXAMPLE
  Veeam_Jobs.ps1
  Veeam_Jobs.ps1 -logDebug
  Veeam_Jobs.ps1 -slowBackupHrs 4 -slowReplicationHrs 2 -slowTapeDays 2 -slowCopyHrs 5
  
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  [Int32]$slowBackupHrs      = 8,
  [Int32]$slowReplicationHrs = 1,
  [Int32]$slowTapeDays       = 3,
  [Int32]$slowCopyHrs        = 8,
  [Switch]$logDebug          = $false
)

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$snapinName                 = "VeeamPSSnapin"

$slowCopyCount              = 0
$slowBackupCount            = 0
$slowReplicationCount       = 0
$slowTapeCount              = 0
$slowJobsCount              = 0
$script:longRunningJobs     = @()
$script:job                 = $null
$script:licenseWarningDays  = 60
$script:licenseCriticalDays = 30

$script:failedJobs          = $false
$script:warningJobs         = $false
$script:successJobs         = $false
$script:i                   = 0

#setup output file for xymon
$script:outputText          = ""
$script:resultOutput        = ""
$script:longJobsOutput      = ""
$script:colour              = $null

$outputFile                 = "C:\Program Files\Xymon\tmp\veeam"
$script:dateString          = Get-Date -Format "dd/MM/yyyy HH:mm:ss"
$script:fqdnHostname        = [System.Net.DNS]::GetHostByName('').HostName.ToLower()
$LogTime                    = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
$logPath                    = "C:\Program Files\Xymon\veeam.log"

# Check OS bit
if ([environment]::Is64BitOperatingSystem){
    $Path = "C:\Program Files\"
} else {
    $Path = "C:\Program Files (x86)\"
}

## End Variable setup

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

function Write-DebugLog {
  param(
    [string]$message,
    [string]$filepath = $logPath
  )
  $message | Out-File $filepath -append
}
## End setup debug logging

## Begin Debug output
if ($logDebug) { Write-DebugLog "============ $LogTime =============" }

## Load VeeamPSSnapin
if (Get-PSSnapin -Registered | ? { $_.Name -eq $snapinName }) {
  if ($logDebug) { Write-DebugLog "Loading VeeamPSSnapin" }
  Add-PSSnapin -Name VeeamPSSnapIn -ErrorAction SilentlyContinue
} else {
  if ($logDebug) { Write-DebugLog "Veeam PSSnapin is not on this system" }
  if ($logDebug) { Write-DebugLog "==============================================" }
  exit
}

#-----------------------------------------------------------[Functions]------------------------------------------------------------

## Set status colour funtion
function Set-Colour
{
    param( [string]$currentColour, [string]$newColour )
    If ($currentColour -eq "red") {
        "red"
    } ElseIf ($newColour -eq "red") {
        "red"
    } ElseIf ($newColour -eq "yellow") {
        "yellow"
    } Else {
        "green"
    }
} # end function

function Build-Header {
  if ($logDebug) { Write-DebugLog "$script:colour colour in build header" }

  $z = $script:colour

  switch ($z) {
    "red" {
      $headerDescription = "There are some critical issues with your veeam jobs, please investigate"
    }
    "yellow" {
      $headerDescription = "There are some warnings with your veeam jobs, please investigate"
    }
    "green" {
      $headerDescription = "All jobs look to have completed successfully"
    }
  }
  $script:outputText = $script:outputText + "$script:colour+12h $script:dateString [$script:fqdnHostname]`r`n"
  $script:outputText = $script:outputText + "`r`n"
  $script:outputText = $script:outputText + "&$script:colour $headerDescription`r`n"
  $script:outputText = $script:outputText + "`r`n"
  $script:outputText = $script:outputText + "$script:ResultsText`r`n"
} # end function

function Build-Output {
  param (
    $status
  )

  $script:i += 1
  $lastSession = $script:job.FindLastSession()
  $jobDuration = $lastSession.Progress.Duration.Ticks
  $bottleNeck = $lastSession.Progress.Bottleneckinfo.BottleNeck
  $bottleNeckPercent = $lastSession.Progress.Bottleneckinfo.$bottleNeck
  $script:colour = Set-Colour $script:colour $status
  $endTime = '{0:yyyy-MM-dd HH:mm:ss}' -f ($lastSession.EndTime)

  $script:resultOutput = $script:resultOutput + "<div class=`"tableBody parent$i`"><div class=`"tableRow`"><div class=`"tableCell center`">&" + $status + "</div><div class=`"tableCell`">" + $script:Job.Name + "</div><div class=`"tableCell`">" + $script:Job.JobType + "</div><div class=`"tableCell`">" + "{0:HH:mm:ss}" -f ([datetime]$jobDuration) + "</div><div class=`"tableCell`">$bottleNeck ($bottleNeckPercent%)</div><div class=`"tableCell`">$endTime</div></div></div>"

} # end function

function Process-Job {

  if (($script:job -ne $null) -and ($script:job.IsRunning -eq $false)) {

    $y = $script:job.GetLastResult()

    switch ($y) {
      "Failed" {
        $script:colour = "red"
        Build-Output $script:colour
      }
      "Warning" {
        $script:colour = "yellow"
        Build-Output $script:colour
      }
      "Success" {
        $script:colour = "green"
        Build-Output $script:colour
      }
    } #end switch
  } # end if

  if (($script:job -ne $null) -and ($script:job.IsRunning -eq $true)) {

    $x = $script:job.FindLastSession().JobType
    $jobDuration = $script:job.FindLastSession().SessionInfo.Progress.Duration
    
    switch ($x) {
      "Backup" {
        $slowBackupDuration = $jobDuration.Hours -gt $slowBackupHrs
        if ($slowBackupDuration) {
          $script:longRunningJobs += $script:job
	      $script:slowJobsCount++
        }
      }
      "Replica" {
        $slowReplicationDuration = $jobDuration.Hours -gt $slowRelicationHrs
        if ($slowReplicationDuration) {
          $script:longRunningJobs += $script:job
	      $script:slowJobsCount++
        }
      }
      "Tape" {
        $slowTapeDuration = $jobDuration.Days -gt $slowTapeDays
        if ($slowTapeDuration) {
          $script:longRunningJobs += $script:job
	      $script:slowJobsCount++
        }
      }
      "Copy" {
        $slowCopyDuration = $jobDuration.Hours -gt $slowCopyHrs
        if ($slowCopyDuration) {
          $script:longRunningJobs += $script:job
	      $script:slowJobsCount++
        }
      }
    } # end switch
  } # end if
} # end function

#-----------------------------------------------------------[Execution]------------------------------------------------------------

# Collect all jobs.
$allResult = Get-VBRJob | where { $_.IsScheduleEnabled -ne $False } | Sort-Object -Property { $_.FindLAstSession().Result },Name

## Begin processing jobs and building output.
Foreach ($script:job in $allResult) {
  if ($script:job.IsRunning) {
    Process-Job
  } else {
    $script:job = $script:job | ? { $_.Findlastsession().EndTime -ge (Get-Date).adddays(-7) }
    Process-Job
  }
}

#Write-Host "There are $script:slowJobsCount slow Jobs"
#Write-Host $script:longRunningJobs

if ($script:slowJobsCount -gt 0) {
  foreach ($longJob in $longRunningJobs) {
    #Build-Output 'yellow'

    $script:i += 1
    $lastSession = $longJob.FindLastSession()
    $jobDuration = $lastSession.Progress.Duration.Ticks
    $bottleNeck = $lastSession.Progress.Bottleneckinfo.BottleNeck
    $bottleNeckPercent = $lastSession.Progress.Bottleneckinfo.$bottleNeck
    $jobStatus = "yellow"
    $script:colour = Set-Colour $script:colour $jobStatus
    $startTime = '{0:yyyy-MM-dd HH:mm:ss}' -f ($lastSession.CreationTime)

    $longJobsOutput = $longJobsOutput + "<div class=`"tableBody parent$i`"><div class=`"tableRow`"><div class=`"tableCell center`">&" + $jobstatus + "</div><div class=`"tableCell`">" + $longJob.Name + "</div><div class=`"tableCell`">" + $longJob.JobType + "</div><div class=`"tableCell`">" + "{0:HH:mm:ss}" -f ([datetime]$jobDuration) + "</div><div class=`"tableCell`">$bottleNeck ($bottleNeckPercent%)</div><div class=`"tableCell`">$startTime</div></div></div>"
  }
}



#Get version and Licenses Info
$veeamExe =  Get-Childitem $Path -Recurse -ErrorAction SilentlyContinue | where { $_.Name -eq "Veeam.Backup.Manager.exe" } | % { $_.FullName }
Write-Host $veeamExe
$VeeamVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($veeamExe).FileVersion
Write-Host $VeeamVersion
$regBinary = (Get-Item 'HKLM:\SOFTWARE\VeeaM\Veeam Backup and Replication\license').GetValue('Lic1')
$veeamLicInfo = [string]::Join($null, ($regBinary | % { [char][int]$_; }))

if($VeeamVersion -like "6*"){
    $pattern = "Expiration date\=\d{1,2}\/\d{1,2}\/\d{1,4}"
}
elseif($VeeamVersion -like "5*"){
    $pattern = "EXPIRATION DATE\=\d{1,2}\/\d{1,2}\/\d{1,4}"
} elseif($VeeamVersion -like "9*"){
    $pattern = "Support expiration date\=\d{1,2}\/\d{1,2}\/\d{1,4}"
}
# Convert Binary key
if($VeeamVersion -like "5*" -OR $VeeamVersion -like "6*" -OR $VeeamVersion -like "9*"){
	$expirationDate = [regex]::matches($VeeamLicInfo, $pattern)[0].Value.Split("=")[1]
    Write-Host $expirationDate
	$totalDaysLeft = ((Get-Date $expirationDate) - (get-date)).Totaldays.toString().split(",")[0]
	$totalDaysLeft = [int]$totalDaysLeft
    Write-Host $totalDaysLeft

	if($totalDaysLeft -lt $AlertDays){
		$script:ResultsText = "&red The Veeam License will expire in $($totalDaysLeft) days"
		Set-Colour $script:colour 'red'
	}
	elseif($totalDaysLeft -lt $WarningDays){
		$script:ResultsText = "&yellow The Veeam License will expire in $($totalDaysLeft) days"
		Set-Colour $script:colour 'yellow'
	}
	else{
		$script:ResultsText = "&green The Veeam License will expire in $($totalDaysLeft) days"
		Set-Colour $script:colour 'green'
	}
}
else{
	$script:ResultsText = "&yellow Unable to process Veeam version"
    Set-Colour $script:colour 'yellow'
}



Build-Header $script:colour

if ($logDebug) { Write-DebugLog "Outputting jobs to table" }
if ($script:slowJobsCount -gt 0) {
  $outputText = $outputText + "<p>&nbsp;</p>`r`n"
  $outputText = $outputText + "<p><strong>Slow running Jobs</strong></p>`r`n"
  #$outputText = $outputText + "<style>table.veeam { border: 1px solid white; border-collapse: collapse; } table.veeam th, table.veeam td { padding: 5px; border-bottom: 1px solid white; }</style>`r`n"
  $outputText = $outputText + "<div class=`"table`"><div class=`"tableHeading`"><div class=`"tableCell`">Result</div><div class=`"tableCell`">Name</div><div class=`"tableCell`">Type</div><div class=`"tableCell`">Duration</div><div class=`"tableCell`">Bottleneck</div><div class=`"tableCell`">Started</div></div>`r`n"
  $outputText = $outputText + $longJobsOutput
  $outputText = $outputText + "</div>`r`n"
}

$outputText = $outputText + "<p>&nbsp;</p>`r`n"
$outputText = $outputText + "<p><strong>Regular Jobs</strong></p>`r`n"
#$outputText = $outputText + "<style>table.veeam { border: 1px solid white; border-collapse: collapse; } table.veeam th, table.veeam td { padding: 5px; border-bottom: 1px solid white; }</style>`r`n"
$outputText = $outputText + "<div class=`"table`"><div class=`"tableHeading`"><div class=`"tableCell`">Result</div><div class=`"tableCell`">Name</div><div class=`"tableCell`">Type</div><div class=`"tableCell`">Duration</div><div class=`"tableCell`">Bottleneck</div><div class=`"tableCell`">Completed</div></div>`r`n"
$outputText = $outputText + $resultOutput
$outputText = $outputText + "</div>`r`n"


if ($logDebug) { Write-DebugLog "Save contents into tmp file" }
$outputText | Set-Content $outputFile

## End Debug output
if ($logDebug) { Write-DebugLog "==============================================" }