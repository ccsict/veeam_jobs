#load Veeam PSSnapin if it isn't already loaded
if ( (Get-PSSnapin -Name VeeamPSSnapIn -ErrorAction SilentlyContinue) -eq $null ) {
  Add-PsSnapin -Name VeeamPSSnapIn -ErrorAction SilentlyContinue
}

function Write-DebugLog {
  param(
    [string]$message,
    [string]$filepath = 'c:\Program Files\Xymon\tapes.log'
  )
  $message | Out-File $filepath -append
}

$LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
Write-DebugLog $LogTime

$outputFile = "C:\Program Files\Xymon\tmp\tapes"
$outputText = ""
$dateString = get-date -Format "dd/MM/yyyy HH:mm:ss"

$tapeJobs = Get-VBRTapeJob

$failedTapeJobs = Get-VBRTapeJob | where { $_.SheduleOptions.Enabled -eq $True -and $_.LastResult -eq "Failed" }
$warningTapeJobs = Get-VBRTapeJob | where { $_.SheduleOptions.Enabled -eq $True -and $_.LastResult -eq "Warning" }

# make red if failed jobs
if (($failedTapeJobs | measure).count -gt 0) {
  Write-DebugLog "Red colour due to failed jobs"
  $colour = "red"
  $outputText = $outputText + "$colour+12h $dateString`r`n"
  $outputText = $outputText + "`r`n"
  $outputText = $outputText + "&red Veeam tape jobs have failed: " + $failedTapeJobs.count + "`r`n"
} elseif (($warningTapeJobs | measure).count -gt 0) {
  
} else {
# make green if none
  Write-DebugLog "Green colour as all jobs were successful"
  $colour = "green"
  $outputText = $outputText + "$colour+12h $dateString`r`n"
  $outputText = $outputText + "`r`n"
  $outputText = $outputText + "&green Look at you go... all Veeam tape jobs have been successful.`r`n"
}


# prepare output
$resultOutput = ""

foreach ($tapeJob in $tapeJobs) {
  $resultOutput = $resultOutput + "<tr><td>" + $tapeJob.Name + "</td><td>" + $tapeJob.LastResult + "</td></tr>"
}

Write-DebugLog "Outputting tape jobs to table"
$outputText = $outputText + "<p>&nbsp;</p>`r`n"
$outputText = $outputText + "<style>table.updates, table.updates th, table.updates td {border: 1px solid white;}</style>`r`n"
$outputText = $outputText + "<table class=`"updates`"><tr><th>Job Name</th><th>Result</th>`r`n"
$outputText = $outputText + $resultOutput
$outputText = $outputText + "</table>`r`n"


Write-DebugLog "Save contents into tmp file"
$outputText | Set-Content $outputFile